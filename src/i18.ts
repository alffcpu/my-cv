export default {
  en: {
    translation: {
      'Hello World': 'Hello World!',
      'Logo': 'Krylov Aleksandr',
      'About me': 'About me',
      'Skills': 'Skills',
      'Offer job': 'Offer job/project',
    }
  },
  ru: {
    translation: {
      'Hello World': 'Привет, Мир!',
      'Logo': 'Крылов Александр',
      'About me': 'Кто я такой?',
      'Skills': 'Компетенции',
      'Offer job': 'Предложить работу/проект',
    }
  },
};
