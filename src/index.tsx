import 'antd/dist/antd.css';
import 'app/styled/global';

import React from 'react';
import ReactDOM from 'react-dom';
import App from './app/App';
import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import resources from './i18';
import moment from 'moment';

const defaultLang = 'en';

i18n
  .use(initReactI18next)
  .init({
    resources,
    lng: defaultLang,
    keySeparator: false,
    fallbackLng: 'en',
    interpolation: {
      escapeValue: false
    }
});

moment.locale(defaultLang);

ReactDOM.render(<App />, document.getElementById('root'));
