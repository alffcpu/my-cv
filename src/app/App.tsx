import React, { Fragment } from 'react';
// import { useTranslation } from 'react-i18next';
import { ConfigProvider } from 'antd';
// import ru_RU from 'antd/lib/locale-provider/ru_RU';
import en_US from 'antd/lib/locale-provider/en_US';
import { createBrowserHistory } from 'history';
import { RouterStore, syncHistoryWithStore } from 'mobx-react-router';
import { Router } from 'react-router';
import Layout from 'app/components/Layout';

const browserHistory = createBrowserHistory();
export const routingStore = new RouterStore();

const history = syncHistoryWithStore(browserHistory, routingStore);

const App: React.FC = () => {
  // const {t} = useTranslation();
  return (<Fragment>
    <ConfigProvider locale={en_US}>
      <Router history={history}>
        <Layout />
      </Router>
    </ConfigProvider>
  </Fragment>);
}

export default App;
