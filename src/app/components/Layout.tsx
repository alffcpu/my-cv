import React from 'react';
import { useTranslation } from 'react-i18next';
import { Layout } from 'antd';
import Header from 'app/components/Header';

const { Content, Footer } = Layout;

const BaseLayout: React.FC = () => {
  const {t} = useTranslation();
  return (<Layout className="layout">
    <Header />
    <Content>
      <div style={{ background: '#fff', padding: 24, minHeight: 280 }}>Content</div>
    </Content>
    <Footer style={{ textAlign: 'center' }}>{t('Hello World')}</Footer>
  </Layout>);
};

export default BaseLayout;
