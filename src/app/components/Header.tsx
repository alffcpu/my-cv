import React from 'react';
import {Avatar, Layout, Menu} from 'antd';
import logo from 'assets/logo.jpg';
import { useTranslation } from 'react-i18next';
import styled from '@emotion/styled';
import { css } from 'emotion';

const { Header } = Layout;

const Logo = styled('div')`
padding-right: 20px;
.logo-title {
  display: inline-block;
  font-size: 24px;
  line-height: 64px;
  padding-left: 10px;
}`;

const menuStyles = css`
  line-height: 64px;
`;

const mainMenu = {
  about: 'About me',
  skills: 'Skills',
  job: 'Offer job/project',
};

const MainMenu: React.FC = () => {
  const {t} = useTranslation();
  return (<Menu
    theme="light"
    mode="horizontal"
    defaultSelectedKeys={['1']}
    className={menuStyles}
  >
    {Object.entries(mainMenu).map(([key, title]) =>
      <Menu.Item key={key}>{t(title)}</Menu.Item>
    )}
  </Menu>);
};

const BaseHeader: React.FC = () => {
  const {t} = useTranslation();
  return (<Header>
    <Logo>
      <Avatar src={logo} />
      <span className="logo-title">{t('Logo')}</span>
    </Logo>
    <MainMenu />
  </Header>);
};

export default BaseHeader;
