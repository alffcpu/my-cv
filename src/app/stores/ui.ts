export const createUiStore = () => ({
  modals: []
});

export type TStore = ReturnType<typeof createUiStore>
