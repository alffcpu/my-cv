import { injectGlobal } from 'emotion';

injectGlobal`
body {
  font-family: 'Fira Sans Condensed';
}
#root {
  min-height: 100vh;
  & > section.layout {
    min-height: 100vh;
  }
  header.ant-layout-header {
    display: flex;
    background-color: #fff;
    border-bottom: 1px solid #e0e0e0;
  }
}
`;
